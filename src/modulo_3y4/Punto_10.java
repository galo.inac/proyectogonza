package modulo_3y4;

import java.util.Scanner;

public class Punto_10 {

	public static void main(String[] args) {
		
Scanner scan = new Scanner(System.in);
		
		int num1, num2, num3;
		
		System.out.print("Ingrese su primer n�mero: ");
		num1 = scan.nextInt();
		System.out.print("Ingrese su segundo n�mero: ");
		num2 = scan.nextInt();
		System.out.print("Ingrese su tercer n�mero: ");
		num3 = scan.nextInt();
		
		if(num1==num2 && num2==num3) {
			System.out.println("Todos los n�meros son iguales");
		}
		else if(num1>num2 && num2==num3) {
			System.out.println("El mayor es el primero");
		}
		else if(num2>num3 && num1==num3) {
			System.out.println("El mayor es el segundo");
		}
		else if(num3>num1 && num1==num2) {
			System.out.println("El mayor es el tercero");
		}
		else if(num1==num2 && num1>num3) {
			System.out.println("Los mayores son el primero y el segundo");
		}
		else if(num1==num3 && num3>num2) {
			System.out.println("Los mayores son el primero y el tercero");
		}
		else if(num2==num3 && num2>num1) {
			System.out.println("Los mayores son el segundo y el tercero");
		}
		else if(num1>num2 && num2>num3) {
			System.out.println("El mayor es el primero");
		}
		else if(num1<num2 && num2<num3) {
			System.out.println("El mayor es el tercero");
		}
		else if(num1<num2 && num2>num3) {
			System.out.println("El mayor es el segundo");
		}
		scan.close();

	}

}
