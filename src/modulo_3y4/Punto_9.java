package modulo_3y4;

import java.util.Scanner;

public class Punto_9 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int jugador1, jugador2;
		
		System.out.println("Piedra, Papel o Tijeras en JAVA con \"and\" (N�mero 0 = Piedra, N�mero 1 = Papel, N�mero 2 = Tijeras)");
		System.out.print("Jugador 1, elija: ");
		jugador1 = scan.nextInt();
		System.out.print("Jugador 2, elija: ");
		jugador2 = scan.nextInt();
		
		if(jugador1==0 && jugador2==0) {
			System.out.println("Empate");
		}
		else if(jugador1==1 && jugador2==1) {
			System.out.println("Empate");
		}
		else if(jugador1==2 && jugador2==2) {
			System.out.println("Empate");
		}
		else if(jugador1==0 && jugador2==1) {
			System.out.println("El jugador 2 gana");
		}
		else if(jugador1==0 && jugador2==2) {
			System.out.println("El jugador 1 gana");
		}
		else if(jugador1==1 && jugador2==0) {
			System.out.println("El jugador 1 gana");
		}
		else if(jugador1==1 && jugador2==2) {
			System.out.println("El jugador 2 gana");
		}
		else if(jugador1==2 && jugador2==0) {
			System.out.println("El jugador 2 gana");
		}
		else if(jugador1==2 && jugador2==1) {
			System.out.println("El jugador 1 gana");
		}
		scan.close();

	}

}
