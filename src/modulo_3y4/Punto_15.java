package modulo_3y4;

import java.util.Scanner;

public class Punto_15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char clase;
		
		System.out.print("Coloque la categor�a de su veh�culo: ");
		clase = scan.next().charAt(0);
		
		switch(clase) {
		case 'a': case 'A':
			System.out.println("Su veh�culo es clase "+clase+", y cuenta con: ");
			System.out.println("\t4 ruedas\n\tUn motor");
			break;
		case 'b': case 'B':
			System.out.println("Su veh�culo es clase "+clase+" y cuenta con: ");
			System.out.println("\t4 ruedas\n\tUn motor\n\tSistema de cerradura centralizada\n\tSistema de aire acondicionado");
			break;
		case 'c': case 'C':
			System.out.println("Su veh�culo clase "+clase+" y cuenta con: ");
			System.out.println("\t4 ruedas\n\tUn motor\n\tSistema de cerradura centralizada\n\tSistema de aire acondicionado\n\tSistema de Airbag");
			break;
		default:
			System.out.println("La clase ingresada es inexistente");
			break;
		}
		
		scan.close();

	}

}
