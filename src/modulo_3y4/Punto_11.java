package modulo_3y4;

import java.util.Scanner;

public class Punto_11 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char letra;
	
		System.out.print("Ingrese una letra: ");
		letra = scan.next().charAt(0);
		
		if(letra=='a'||letra=='e'||letra=='i'||letra=='o'||letra=='u') {
			System.out.println(letra+" es una vocal");
		}
		else {
			System.out.println(letra+" es una consonante");
		}
		scan.close();

	}

}
