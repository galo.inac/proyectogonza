package modulo_3y4;

import java.util.Scanner;

public class Punto_4 {

	public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			
			System.out.print("Ingrese la categor�a: "); 
			String cat = scan.nextLine();

			if(cat.equals("a")) {
				System.out.println("La categor�a \"" + cat + "\" es de hijo.");
			}
			else if(cat.equals("b")){
				System.out.println("La categor�a \"" + cat + "\" es de padre/madre.");
			}
			else if(cat.equals("c")) {
				System.out.println("La categor�a \"" + cat + "\" es de abuelo/abuela.");
			}
			else {
				System.out.println("La categor�a \"" + cat + "\" no es v�lida.");
			}
			scan.close();

	}

}
